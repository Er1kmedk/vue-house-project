import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Home from './views/Home.vue'
import House from './views/House.vue'
import Agent from './views/Agent.vue'
import 'bootstrap/dist/css/bootstrap.min.css'



Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
	{ path: '/', component: Home},
	{ path: '/House?agent=:agentIndex&house=:houseIndex', name:"House", component: House}, 
	{ path: '/Agent?agent=:agentIndex', name:"Agent", component: Agent}
]

const router = new VueRouter({ routes })

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
